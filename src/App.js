import {Container} from 'react-bootstrap'
import {Fragment} from 'react'
import AppNavbar from './Components/AppNavbar';
// import Banner from './Components/Banner'
// import Highlights from './Components/Highlights'
import Home from './pages/home'
import './App.css';

import Courses from './pages/courses'



function App() {
  return (
    <Fragment>
     <AppNavbar/>
        <Container>
            <Home/>
            <Courses/>
       </Container>
    </Fragment>
  );
}

export default App;
